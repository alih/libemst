/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "emst.h"
#include "emst-geometry.h"

#define NEXT_MOD3(K) (((K) + 1) % 3)
#define PREV_MOD3(K) (((K) + 2) % 3)

static bool left_of(
    const EmstTriangle *restrict triangles,
    const int16_t *restrict coords,
    int a_t, int a_i,
    int e_t, int e_i)
{
    int a = triangles[a_t].vertices[a_i];
    int b = triangles[e_t].vertices[NEXT_MOD3(e_i)];
    int c = triangles[e_t].vertices[PREV_MOD3(e_i)];
    return emst_is_counterclockwise_from_array(coords, a, b, c);
}


//      _______
//     /\ old /
//    /  \   /
//   /new \ /
//  /______. <- *p_i
//
static void ccw_around_vertex(
    const EmstTriangle *restrict triangles, 
    int *restrict p_t, 
    int *restrict p_i)
{
    int i = NEXT_MOD3(*p_i);
    int t = triangles[*p_t].edges_t[i];
    i = triangles[*p_t].edges_i[i];
    *p_t = t;
    *p_i = NEXT_MOD3(i);
}

/*       _______
 *       \ old /\
 *        \   /  \
 *         \ / new\
 *  *p_i -> .______\
 */

static void cw_around_vertex(
    const EmstTriangle *restrict triangles, 
    int *restrict p_t, 
    int *restrict p_i)
{
    int i = PREV_MOD3(*p_i);
    int t = triangles[*p_t].edges_t[i];
    i = triangles[*p_t].edges_i[i];
    *p_t = t;
    *p_i = PREV_MOD3(i);
}

/**
 * Make 2 triangles point to each other.
 */
static void bind(EmstTriangle *triangles, int t1, int i1, int t2, int i2)
{
    triangles[t1].edges_t[i1] = t2;
    triangles[t1].edges_i[i1] = i2;
    triangles[t2].edges_t[i2] = t1;
    triangles[t2].edges_i[i2] = i1;
}

#if 0
//#ifdef NDEBUG
//#define assert_triangle_is_sane(A, B) ((void) (B))
//#define assert_triangles_are_sane(A, B) ((void) (B))
//#else
static void assert_triangle_is_sane(EmstTriangle *triangles, int t)
{
    EmstTriangle *T = triangles + t;
    assert(T->vertices[0] != T->vertices[1]);
    assert(T->vertices[1] != T->vertices[2]);
    assert(T->vertices[0] != T->vertices[2]);

    EmstTriangle *T0 = triangles + T->edges_t[0];
    EmstTriangle *T1 = triangles + T->edges_t[1];
    EmstTriangle *T2 = triangles + T->edges_t[2];
    assert(T0 != T);
    assert(T1 != T);
    assert(T2 != T);
    // by the way, it could be that T0 == T1 == T2.

    int i0 = T->edges_i[0];
    int i1 = T->edges_i[1];
    int i2 = T->edges_i[2];

    // vertex agreement
    assert(T0->vertices[PREV_MOD3(i0)] == T->vertices[1]);
    assert(T0->vertices[NEXT_MOD3(i0)] == T->vertices[2]);
    assert(T1->vertices[PREV_MOD3(i1)] == T->vertices[2]);
    assert(T1->vertices[NEXT_MOD3(i1)] == T->vertices[0]);
    assert(T2->vertices[PREV_MOD3(i2)] == T->vertices[0]);
    assert(T2->vertices[NEXT_MOD3(i2)] == T->vertices[1]);

    // mutual pointing
    assert(T0->edges_t[i0] == t);
    assert(T0->edges_i[i0] == 0);
    assert(T1->edges_t[i1] == t);
    assert(T1->edges_i[i1] == 1);
    assert(T2->edges_t[i2] == t);
    assert(T2->edges_i[i2] == 2);
}

static void assert_triangles_are_sane(EmstTriangle *triangles, int n_vertices)
{
    int n_triangles = 2 * n_vertices - 2;
    for (int i = 0; i < n_triangles; i++)
        assert_triangle_is_sane(triangles, i);
}

#endif // ifndef NDEBUG

/* Change the triangulation:
 *
 *    old:        new:
 *     .           .
 *    / \         /|\
 *   /   \       / | \
 *  /_____\     /  |  \
 *  \     /     \  |t1/
 *   \t1 /       \ | /
 *    \ /         \|/
 *     O <- i1     O
 *
 */
static void flip(EmstTriangle *restrict triangles, int t1, int i1)
{
    EmstTriangle *T1 = triangles + t1;
    int t2 = T1->edges_t[i1];
    EmstTriangle *T2 = triangles + t2;
    int i2 = T1->edges_i[i1];

    // store the neighborhood in quad_t and quad_i
    int quad1_t, quad1_i, quad3_t, quad3_i; // quad[2]   .   quad[1]
                                            //          / \   ("\" can't be the
    quad1_t = T2->edges_t[NEXT_MOD3(i2)];   //         /   \   last char here)
    quad1_i = T2->edges_i[NEXT_MOD3(i2)];   //         \   /
    quad3_t = T1->edges_t[NEXT_MOD3(i1)];   // quad[3]  \ /  quad[0]
    quad3_i = T1->edges_i[NEXT_MOD3(i1)];   //           O

    // rewire the vertices
    assert(T1->vertices[PREV_MOD3(i1)] == T2->vertices[NEXT_MOD3(i2)]);
    assert(T1->vertices[NEXT_MOD3(i1)] == T2->vertices[PREV_MOD3(i2)]);
    T1->vertices[PREV_MOD3(i1)] = T2->vertices[i2];
    T2->vertices[PREV_MOD3(i2)] = T1->vertices[i1];

    // reassign the triangles' neighborhood
    bind(triangles, t1, NEXT_MOD3(i1), t2, NEXT_MOD3(i2));
    bind(triangles, t1, i1, quad1_t, quad1_i);
    bind(triangles, t2, i2, quad3_t, quad3_i);
}

// ______________________   Delaunay following [GS]   _________________________

/* 
 * Euler's formula: V - E + F = 2
 * All our faces are triangles. Let k = F/2. Then
 *   E = 3k
 *   F = 2k
 *   V - E + F = V - 3k + 2k = V - k = 2
 *   Therefore, k = V - 2, F = 2V - 4, E = 3V - 6.
 * Here V = n_points + 1 (+1 for the virtual point at infinity),
 * so F = 2(n_points + 1) - 4 = 2 * n_points - 2.
 *
 * Most of the functions below receive the two arguments:
 *
 *     EmstTriangle *triangles,
 *     const int16_t *coords
 *
 * Every point occupies 2 values in the coords array.
 * 
 *
 * The same offset for the triangles and coords arrays.
 * Assume that the task is to output 2 * n_points triangles,
 * leaving the last 2 uninitialized. So we have 2 * n_points coordinates 
 * and the same amount of triangles. 
 */


static void lower_common_tangent(
    const EmstTriangle *restrict triangles,
    const int16_t *restrict coords,
    int *restrict p_ldi_t,
    int *restrict p_ldi_i,
    int *restrict p_rdi_t,
    int *restrict p_rdi_i)
{
    // The corresponding fragment from [GS]:
    //    {Compute the lower common tangent of L and R:}
    //    DO
    //      IF LeftOf(rdi.Org, ldi) THEN ldi = ldi.Lnext
    //      ELSIF RightOf(ldi.Org, rdi) THEN rdi = rdi.Rprev
    //      ELSE EXIT FI
    //    OD;
    while (true)
    {
        if (left_of(triangles, coords, *p_rdi_t, PREV_MOD3(*p_rdi_i),
                 *p_ldi_t, *p_ldi_i))
        {
            ccw_around_vertex(triangles, p_ldi_t, p_ldi_i);
        }
        else if (left_of(triangles, coords, *p_ldi_t, NEXT_MOD3(*p_ldi_i),
                 *p_rdi_t, *p_rdi_i)) // in [GS] it's RightOf since rdi is CW
        {
            cw_around_vertex(triangles, p_rdi_t, p_rdi_i);
        }
        else
            break;
    }
}


static void delaunay_2(EmstTriangle *t, int k, 
    int *out_l_t, int *out_l_i,
    int *out_r_t, int *out_r_i)
{
    // Picture a sphere divided by an equator into 2 hemispheres.
    // The equator is divided into 3 parts by 3 points,
    // one of which is infinity, others are our 2 points.
    // There's another illustration in [Sh].

    t[k].vertices[0] = -1;
    t[k].vertices[1] = k + 2;
    t[k].vertices[2] = k;
    t[k + 1].vertices[0] = -1;
    t[k + 1].vertices[1] = k;
    t[k + 1].vertices[2] = k + 2;

    t[k].edges_t[0] = k + 1;
    t[k].edges_t[1] = k + 1;
    t[k].edges_t[2] = k + 1;
    t[k + 1].edges_t[0] = k;
    t[k + 1].edges_t[1] = k;
    t[k + 1].edges_t[2] = k;

    t[k].edges_i[0] = 0;
    t[k].edges_i[1] = 2;
    t[k].edges_i[2] = 1;
    t[k + 1].edges_i[0] = 0;
    t[k + 1].edges_i[1] = 2;
    t[k + 1].edges_i[2] = 1;

    *out_l_t = *out_r_t = k;
    *out_l_i = *out_r_i = 0;
}

static void delaunay_3(
    EmstTriangle *restrict t,
    const int16_t *restrict coords,
    int k,
    int *out_l_t,
    int *out_l_i,
    int *out_r_t,
    int *out_r_i)
{
    // -1_______c_______-1
    //   \     / \     /
    //    \k+1/   \ k /
    //     \ / k+3 \ /
    //      a_______b
    //       \     /
    //        \k+2/
    //         \ /
    //         -1
    //
    // See [Sh] for an illustration with pointers shown as arrows.

    const int a = k;
    int b, c;
    if (emst_is_counterclockwise(k,      coords[k], coords[k + 1],
                                k + 2,  coords[k + 2], coords[k + 3],
                                k + 4,  coords[k + 4], coords[k + 5]))
    {
        b = k + 2;
        c = k + 4;
        *out_r_t = k;
    }
    else
    {
        b = k + 4;
        c = k + 2;
        *out_r_t = k + 2;
    }
    
    // now abc is CCW
    EmstTriangle *t0 = t + (k + 3);
    t0->vertices[0] = a;
    t0->vertices[1] = b;
    t0->vertices[2] = c;

    EmstTriangle *ta = t + k;
    EmstTriangle *tb = t + (k + 1);
    EmstTriangle *tc = t + (k + 2);

    ta->vertices[0] = tb->vertices[0] = tc->vertices[0] = -1;
    ta->vertices[1] = c;
    ta->vertices[2] = b;
    tb->vertices[1] = a;
    tb->vertices[2] = c;
    tc->vertices[1] = b;
    tc->vertices[2] = a;

    for (int i = 0; i < 3; i++)
    {
        t0->edges_t[i] = k + i;
        t0->edges_i[i] = 0;
        t[k + i].edges_t[0] = k + 3;
        t[k + i].edges_i[0] = i;
        t[k + i].edges_t[1] = k + PREV_MOD3(i);
        t[k + i].edges_i[1] = 2;
        t[k + i].edges_t[2] = k + NEXT_MOD3(i);
        t[k + i].edges_i[2] = 1;
    }

    *out_l_t = k + 2;
    *out_l_i = *out_r_i = 0;
}

static bool delete_edges(EmstTriangle *restrict triangles, 
                         const int16_t *restrict coords,
                         int base_t, int base_i,
                         int turn) // 1 for right, 2 for left
{
    /*
     * This picture if for the left case:
     *
     *       lcand (the vertical edge)
     *         |
     *   ______v 
     *   \     |\
     *    \    | \ <- support triangle
     *     \   |  \ <- infinity point
     *      \  |  /\
     * lcand.\ | /  \ <- base triangle
     * Onext  \|/____\
     *           basel
     */

    int support_t = triangles[base_t].edges_t[(base_i + turn) % 3];
    int support_i = triangles[base_t].edges_i[(base_i + turn) % 3];
    if (triangles[support_t].vertices[support_i] < 0
     || !left_of(triangles, coords, support_t, support_i, base_t, base_i))
        return false;
    
    bool flipped = false;
    while (true)
    {
        int lcand_i = (support_i + turn) % 3;
        int next_t = triangles[support_t].edges_t[lcand_i];
        int next_i = triangles[support_t].edges_i[lcand_i];
        if (triangles[next_t].vertices[next_i] < 0)
            break; // this is not in [GS], but otherwise doesn't work

        if (!emst_is_in_circle_from_array(coords,
                triangles[base_t].vertices[NEXT_MOD3(base_i)],
                triangles[base_t].vertices[PREV_MOD3(base_i)],
                triangles[support_t].vertices[support_i],
                triangles[next_t].vertices[next_i]))
            break;

        flip(triangles, next_t, next_i);

        // XXX: code copy from the top of this function
        support_t = triangles[base_t].edges_t[(base_i + turn) % 3];
        support_i = triangles[base_t].edges_i[(base_i + turn) % 3];
        flipped = true;
    }

    if (!flipped)
        return true;
    return triangles[support_t].vertices[support_i] >= 0
      && left_of(triangles, coords, support_t, support_i, base_t, base_i);
}

// This is the part from [GS] from the comment:
//
//  "The next cross edge is to be connected to either lcand.Dest or rcand.Dest.
//  If both are valid, then choose the appropriate one using the InCircle test:"

static void new_cross_edge(
    EmstTriangle *restrict triangles, 
    const int16_t *restrict coords,
    int *restrict p_base_t,
    int *restrict p_base_i,
    int valid_lcand,
    int valid_rcand)
{
    int base_t = *p_base_t;
    int base_i = *p_base_i;
    int left_t = triangles[base_t].edges_t[PREV_MOD3(base_i)];
    int left_i = triangles[base_t].edges_i[PREV_MOD3(base_i)];
    int right_t = triangles[base_t].edges_t[NEXT_MOD3(base_i)];
    int right_i = triangles[base_t].edges_i[NEXT_MOD3(base_i)];
    if (!valid_lcand || (valid_rcand && emst_is_in_circle_from_array(coords,
            triangles[left_t].vertices[left_i],
            triangles[base_t].vertices[NEXT_MOD3(base_i)],
            triangles[base_t].vertices[PREV_MOD3(base_i)],
            triangles[right_t].vertices[right_i]
        )))
    {
        // connect to the right
        flip(triangles, base_t, NEXT_MOD3(base_i));
        *p_base_t = triangles[base_t].edges_t[PREV_MOD3(base_i)];
        *p_base_i = triangles[base_t].edges_i[PREV_MOD3(base_i)];
    }
    else
    {
        // connect to the left
        flip(triangles, base_t, PREV_MOD3(base_i));
    }
}


// This is the merge loop from [GS].
static void merge(
    EmstTriangle *restrict triangles,
    const int16_t *restrict coords,
    int base_t,
    int base_i)
{
    while (true)
    {
        bool valid_lcand = delete_edges(triangles, coords, base_t, base_i, 2);
        bool valid_rcand = delete_edges(triangles, coords, base_t, base_i, 1);
        if (!valid_lcand && !valid_rcand)
            break;
        new_cross_edge(triangles, coords, &base_t, &base_i, 
                               valid_lcand, valid_rcand);
    }
}

/*
 * Join 2 connected components with an edge,
 * using 2 new triangles:
 *
 *      |\      /|            |\  /|
 *      | \    / |            | \/ |
 *      | /    \ |            | /\ |
 *      |/      \|            |/__\|
 *      |\      /|       ->   |\  /|
 *      | \    / |            | \/ |
 *  ll  | /    \ | rl         | /\ |
 *      |/      \|            |/  \|
 *
 * In this picture, the horizontal and vertical edges are real,
 * the diagonal ones connect to infinity.
 *
 */
static void join(
    EmstTriangle *restrict triangles,
    int ll_t,
    int ll_i,
    int rl_t,
    int rl_i,
    int offset)
{
    int lu_t = triangles[ll_t].edges_t[PREV_MOD3(ll_i)];
    int lu_i = triangles[ll_t].edges_i[PREV_MOD3(ll_i)];
    int ru_t = triangles[rl_t].edges_t[NEXT_MOD3(rl_i)];
    int ru_i = triangles[rl_t].edges_i[NEXT_MOD3(rl_i)];

    EmstTriangle *upper = triangles + offset;
    EmstTriangle *lower = triangles + offset + 1;

    upper->vertices[0] = -1;
    upper->vertices[1] = triangles[ll_t].vertices[NEXT_MOD3(ll_i)];
    upper->vertices[2] = triangles[rl_t].vertices[PREV_MOD3(rl_i)];
    lower->vertices[0] = -1;
    lower->vertices[1] = upper->vertices[2];
    lower->vertices[2] = upper->vertices[1];

    upper->edges_t[0] = offset + 1;
    upper->edges_i[0] = 0;
    lower->edges_t[0] = offset;
    lower->edges_i[0] = 0;

    bind(triangles, ru_t, ru_i, offset, 1);
    bind(triangles, lu_t, lu_i, offset, 2);
    bind(triangles, ll_t, PREV_MOD3(ll_i), offset + 1, 1);
    bind(triangles, rl_t, NEXT_MOD3(rl_i), offset + 1, 2);
}


static void delaunay_rec(
    EmstTriangle *restrict triangles,
    const int16_t *restrict coords,
    int offset,
    int n_points,
    int *restrict out_l_t,
    int *restrict out_l_i,
    int *restrict out_r_t,
    int *restrict out_r_i)
{
    if (n_points <= 3)
    {
        if (n_points == 3)
        {
            delaunay_3(triangles, coords, offset,
                        out_l_t, out_l_i, out_r_t, out_r_i);
            assert(triangles[*out_r_t].vertices[NEXT_MOD3(*out_r_i)] == offset + 2 * n_points - 2);
        }
        else
        {
            assert(n_points == 2);
            delaunay_2(triangles, offset, out_l_t, out_l_i, out_r_t, out_r_i);
            assert(triangles[*out_r_t].vertices[NEXT_MOD3(*out_r_i)] == offset + 2 * n_points - 2);
        }
        return;
    }

    int n1 = n_points / 2;
    int n2 = n_points - n1;
    int ldi_t, ldi_i;
    int rdi_t, rdi_i;
    delaunay_rec(triangles, coords, offset, n1, 
                    out_l_t, out_l_i, &ldi_t, &ldi_i);
    delaunay_rec(triangles, coords, offset + 2 * n1, n2,
                    &rdi_t, &rdi_i, out_r_t, out_r_i);

    lower_common_tangent(triangles, coords, &ldi_t, &ldi_i, &rdi_t, &rdi_i);
    int upper = offset + 2 * n1 - 2;
    int lower = upper + 1;
    assert(triangles[*out_r_t].vertices[NEXT_MOD3(*out_r_i)] == offset + 2 * n_points - 2);
    join(triangles, ldi_t, ldi_i, rdi_t, rdi_i, upper);
    if (triangles[lower].vertices[2] == offset)
    {
        *out_l_t = lower;
        *out_l_i = 0;
    }
    if (triangles[lower].vertices[1] == offset + 2 * n_points - 2)
    {
        *out_r_t = lower;
        *out_r_i = 0;
    }

    assert(triangles[*out_r_t].vertices[NEXT_MOD3(*out_r_i)] == offset + 2 * n_points - 2);
    merge(triangles, coords, upper, 0);

    assert(triangles[*out_l_t].vertices[*out_l_i] == -1);
    assert(triangles[*out_l_t].vertices[PREV_MOD3(*out_l_i)] == offset);
    assert(triangles[*out_r_t].vertices[*out_r_i] == -1);
    assert(triangles[*out_r_t].vertices[NEXT_MOD3(*out_r_i)] == offset + 2 * n_points - 2);
}


static void iota(int *a, int n)
{
    for (int i = 0; i < n; i++)
        a[i] = i;
}

// __________________________   heapsort   ______________________

// We must sort by y because of the perturbation terms in [Kn].
// Also, we should sort points with the same y just to make sure
// that splitting the array of points corresponds to some line.
// (This is not discussed in [GS], as it's assumed that no 3 points
//  are collinear)
#define EMST_HEAP_IS_ABOVE(A, B) \
    (coords[2 * perm[A] + 1] > coords[2 * perm[B] + 1] \
  || (coords[2 * perm[A] + 1] == coords[2 * perm[B] + 1] \
      && \
      coords[2 * perm[A]] > coords[2 * perm[B]]) \
    )
#define EMST_HEAP_SWAP(A, B) do { \
    int tmp = perm[A]; \
    perm[A] = perm[B]; \
    perm[B] = tmp; } while(0)
#define EMST_HEAP_BUILD_PROTO static void heap_build( \
    int *restrict perm, const int16_t *restrict coords, int n)
#define EMST_HEAP_EXTRACT_PROTO static void heap_extract( \
    int *restrict perm, const int16_t *restrict coords, int n)
#include "emst-heap.incl"

static void heapsort(int *restrict perm, const int16_t *restrict coords, int n)
{
    heap_build(perm, coords, n);
    while (n)
        heap_extract(perm, coords, n--);
}

#if 0
static void save_coords(const char *path, const int16_t *coords, int n_points)
{
    FILE *f = fopen(path, "w");
    fprintf(f, "%d\n", n_points);
    for (int i = 0; i < n_points; i++)
        fprintf(f, "%d %d\n", coords[2 * i], coords[2 * i + 1]);
    fclose(f);
}

static int16_t *load_coords(const char *path, int *out_n_points)
{
    FILE *f = fopen(path, "r");
    int n;
    fscanf(f, "%d", &n);
    *out_n_points = n;
    int16_t *coords = (int16_t *) malloc(2 * n * sizeof(int16_t));
    for (int i = 0; i < n; i++)
    {
        int x, y;
        fscanf(f, "%d %d", &x, &y);
        coords[2 * i] = x;
        coords[2 * i + 1] = y;
    }
    fclose(f);
    return coords;
}
#endif

EmstTriangle *emst_delaunay(
    const int16_t *coords,
    int n_points)
{
    if (n_points < 2)
        return NULL;

    int *perm = (int *) malloc(n_points * sizeof(int));
    iota(perm, n_points);
    heapsort(perm, coords, n_points);
    int16_t *sorted_coords = (int16_t *) malloc(2 * n_points * sizeof(int16_t));
    for (int i = 0; i < n_points; i++)
    {
        sorted_coords[2 * i] = coords[2 * perm[i]];
        sorted_coords[2 * i + 1] = coords[2 * perm[i] + 1];
        assert(i == 0 || sorted_coords[2 * i + 1] >= sorted_coords[2 * i - 1]);
    }

    // save_coords("all-points.dat", sorted_coords, n_points);
 
    int n_triangles = 2 * n_points - 2;
    EmstTriangle *triangles = (EmstTriangle *) malloc(n_triangles * sizeof(EmstTriangle));
  
    int l_t, l_i, r_t, r_i; 
    delaunay_rec(triangles, sorted_coords, 0, n_points, &l_t, &l_i, &r_t, &r_i);
    for (int i = 0; i < n_triangles; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (triangles[i].vertices[j] >= 0)
                triangles[i].vertices[j] = perm[triangles[i].vertices[j] / 2];
        }
    }

    free(sorted_coords);
    free(perm);
    
    //free(triangles);
    return triangles;
}

#if 0

#include "emst-visual.h"
int main()
{
    int n_points;
    int16_t *coords = load_coords("all-points.dat", &n_points);
    EmstTriangle *triangles = emst_delaunay(coords, n_points);
    emst_visualize_triangulation_to_png("foo.png", NULL, triangles, coords, n_points);
    free(triangles);
    free(coords);
    return 0;
}

#endif
