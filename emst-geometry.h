/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef EMST_GEOMETRY_H
#define EMST_GEOMETRY_H

#include <stdbool.h>
#include <stdint.h>

bool emst_x_lt(int i1, int x1, int i2, int x2);
bool emst_y_lt(int i1, int y1, int i2, int y2);

int emst_counterclockwise_det(
    int ax, int ay, 
    int bx, int by, 
    int cx, int cy);

bool emst_is_counterclockwise(
    int pi, int px, int py, 
    int qi, int qx, int qy, 
    int ri, int rx, int ry);

bool emst_is_counterclockwise_from_array(
    const int16_t *coords, 
    int p, int q, int r);

int64_t emst_in_circle_det(
    int ax, int ay, 
    int bx, int by, 
    int cx, int cy,
    int dx, int dy);

bool emst_is_in_circle(
    int pi, int px, int py, 
    int qi, int qx, int qy, 
    int ri, int rx, int ry,
    int si, int sx, int sy);

bool emst_is_in_circle_from_array(
    const int16_t *coords, 
    int p, int q, int r, int s);


#endif
