/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include "emst-geometry.h"

#ifndef NDEBUG
// We only allow 14-bit integers to prevent overflows.
static int emst_in_geometry_range(int a)
{
    return a >= -8192 && a <= 8191;
}

static int emst_point_in_geometry_range(int a, int b)
{
    return emst_in_geometry_range(a)
        && emst_in_geometry_range(b);
}
#endif

// See (14.16)
bool emst_x_lt(int i1, int x1, int i2, int x2)
{
    if (x1 < x2)
        return true;
    if (x1 > x2)
        return false;
    if (i1 > i2)
        return true;
    return false; 
}

bool emst_y_lt(int i1, int y1, int i2, int y2)
{
    if (y1 < y2)
        return true;
    if (y1 > y2)
        return false;
    if (i1 < i2)
        return true;
    return false; 
}

int emst_counterclockwise_det(
    int ax, int ay, 
    int bx, int by, 
    int cx, int cy)
{
    assert(emst_point_in_geometry_range(ax, ay));
    assert(emst_point_in_geometry_range(bx, by));
    assert(emst_point_in_geometry_range(cx, cy));
    
    // we're calculating this:
    // | ax ay 1 |   | ax - cx  ay - cy  1 |   | ax - cx  ay - cy |
    // | bx by 1 | = | bx - cx  by - cy  1 | = | bx - cx  by - cy |
    // | cx cy 1 |   |    0        0     1 |   
    // (subtract the properly multiplied last column)

    // |ax - cx| < 2^15 (actually even 2^14)
    // |(ax - cx)(by - cy)| < 2^30
    // |result| < 2^31
    return ((int) ax - cx) * ((int) by - cy) 
         - ((int) ay - cy) * ((int) bx - cx);
}

bool emst_is_counterclockwise(
    int pi, int px, int py, 
    int qi, int qx, int qy, 
    int ri, int rx, int ry)
{
    // See Knuth, "Axioms and Hulls", end of chapter 14
    int det = emst_counterclockwise_det(px, py, qx, qy, rx, ry);
    if (det > 0)
        return 1;
    if (det < 0)
        return 0;
    
    // Step 2
    int b = 1;
    int t;
    while (!(pi < qi && qi < ri))
    {
        if (pi > qi)
        {
            t = pi; pi = qi; qi = t;
            t = px; px = qx; qx = t;
            t = py; py = qy; qy = t;
            b = 1 - b;
        }
        if (qi > ri)
        {
            t = qi; qi = ri; ri = t;
            t = qx; qx = rx; rx = t;
            t = qy; qy = ry; ry = t;
            b = 1 - b;
        }
    }

    // Step 3
    if (px > qx 
     || (px == qx && py > qy)
     || (px == qx && py == qy && rx > px)
     || (px == qx && py == qy && rx == px && ry >= py))
        return 1 - b;
    return b;
}

int64_t emst_in_circle_det(
    int ax, int ay, 
    int bx, int by, 
    int cx, int cy,
    int dx, int dy)
{
    assert(emst_point_in_geometry_range(ax, ay));
    assert(emst_point_in_geometry_range(bx, by));
    assert(emst_point_in_geometry_range(cx, cy));
    assert(emst_point_in_geometry_range(dx, dy));

    // we're calculating this (see 17.3 from Knuth, "Axioms and Hulls"):
    //
    // | ax  ay  ax^2 + ay^2  1 |       
    // | bx  by  bx^2 + by^2  1 |
    // | cx  cy  cx^2 + cy^2  1 |
    // | dx  dy  dx^2 + dy^2  1 |
    //
    //  = (subtract the properly multiplied last column)
    //
    // | ax - dx  ay - dy  ax^2 + ay^2 - dx^2 - dy^2  1 |
    // | bx - dx  by - dy  bx^2 + by^2 - dx^2 - dy^2  1 |
    // | cx - dx  cy - dy  cx^2 + cy^2 - dx^2 - dy^2  1 |
    // |    0        0                 0              1 | 
    //
    //  =
    //
    // | ax - dx  ay - dy  ax^2 + ay^2 - dx^2 - dy^2 |
    // | bx - dx  by - dy  bx^2 + by^2 - dx^2 - dy^2 |
    // | cx - dx  cy - dy  cx^2 + cy^2 - dx^2 - dy^2 |
    //
    //  = (subtract the first and second column multiplied by -2dx, -2dy)
    //
    // | ax - dx  ay - dy  (ax - dx)^2 + (ay - dy)^2 |
    // | bx - dx  by - dy  (bx - dx)^2 + (by - dy)^2 |
    // | cx - dx  cy - dy  (cx - dx)^2 + (cy - dy)^2 |
    
    int xa = ax - dx; // |xa| < 2^14
    int ya = ay - dy;
    int xb = bx - dx;
    int yb = by - dy;
    int xc = cx - dx;
    int yc = cy - dy;
    int za = xa * xa + ya * ya; // |za| < 2^29
    int zb = xb * xb + yb * yb; 
    int zc = xc * xc + yc * yc;
    
    // | xa ya za |
    // | xb yb zb |
    // | xc yc zc |
    return (int64_t) xa * yb * zc // |term| < 2^14 * 2^14 * 2^29 = 2^57
         + (int64_t) ya * zb * xc
         + (int64_t) za * xb * yc
         - (int64_t) xa * yc * zb
         - (int64_t) ya * xb * zc
         - (int64_t) za * yb * xc;
}

// See Knuth, "Axioms and Hulls", (19.11), (19.12)
static int64_t knuth_fg(
    int px, int py,
    int qx, int qy,
    int rx, int ry,
    int sx, int sy)
{
    int psx = px - sx;  // | | < 2^14
    int psy = px - sy;
    int ps2 = psx * psx + psy * psy; // | | < 2^29
    int qsx = qx - sx;
    int qsy = qx - sy;
    int qs2 = qsx * qsx + qsy * qsy;
    int rsx = rx - sx;
    int rsy = ry - sy;
    int rs2 = rsx * rsx + rsy * rsy;

    int64_t a01 = ps2 - rs2; // | | < 2^30
    int64_t a11 = qs2 - rs2;

    int64_t f = (px - rx) * a11 - (qx - rx) * a01; // | | < 2^45
    if (f)
        return f;

    return (py - ry) * a11 - (qy - ry) * a01;
}

// See (19.14)
static int knuth_j(
    int px, int py,
    int qx, int qy,
    int rx, int ry,
    int sx, int sy)
{
    (void) ry;
    (void) sx;
    int qrx = qx - rx; // | | < 2^14
    int qsy = qy - sy;
    int prx = px - rx;
    int psy = py - sy;
    return qrx * qrx + qsy * qsy - prx * prx - psy * psy; // | | < 2^30
}

// See (19.13)
static int knuth_hj(
    int px, int py,
    int qx, int qy,
    int rx, int ry,
    int sx, int sy)
{
    int h = (qx - px) * (ry - sy);
    if (h)
       return h; 
    return knuth_j(px, py, qx, qy, rx, ry, sx, sy);
}

bool emst_is_in_circle(
    int pi, int px, int py, 
    int qi, int qx, int qy, 
    int ri, int rx, int ry,
    int si, int sx, int sy)
{
    assert(pi != qi && pi != ri && pi != si);
    assert(qi != ri && qi != si && ri != si);

    // See Knuth, "Axioms and Hulls", chapter 19
    // Step 1 (19.9)
    int64_t det = emst_in_circle_det(px, py, qx, qy, rx, ry, sx, sy);
    if (det > 0)
        return 1;
    if (det < 0)
        return 0;

    // Step 2
    int result = 1;
    int t;
    while (!(pi < qi && qi < ri && ri < si))
    {
        if (pi > qi)
        {
            t = pi; pi = qi; qi = t;
            t = px; px = qx; qx = t;
            t = py; py = qy; qy = t;
            result = 1 - result;
        }
        if (qi > ri)
        {
            t = qi; qi = ri; ri = t;
            t = qx; qx = rx; rx = t;
            t = qy; qy = ry; ry = t;
            result = 1 - result;
        }
        if (ri > si)
        {
            t = ri; ri = si; si = t;
            t = rx; rx = sx; sx = t;
            t = ry; ry = sy; sy = t;
            result = 1 - result;
        }
    }

    // Step 3
    // See (19.10)
    int64_t k;
    if (!(k = knuth_fg(px, py, qx, qy, rx, ry, sx, sy))
     && !(k = knuth_fg(qx, qy, px, py, sx, sy, rx, ry))
     && !(k = knuth_fg(rx, ry, sx, sy, px, py, qx, qy))
     && !(k = knuth_hj(px, py, qx, qy, rx, ry, sx, sy))
     && !(k = knuth_hj(rx, ry, px, py, qx, qy, sx, sy))
     && !(k = knuth_j(px, py, sx, sy, qx, qy, rx, ry)))
        k = 1;

    return k > 0 ? result : 1 - result;
}

bool emst_is_counterclockwise_from_array(
    const int16_t *coords, 
    int p, int q, int r)
{
    assert(p >= 0);
    assert(q >= 0);
    assert(r >= 0);
    return emst_is_counterclockwise(
        p, coords[p], coords[p + 1],
        q, coords[q], coords[q + 1],
        r, coords[r], coords[r + 1]);
}
bool emst_is_in_circle_from_array(
    const int16_t *coords, 
    int p, int q, int r, int s)
{
    assert(p >= 0);
    assert(q >= 0);
    assert(r >= 0);
    assert(s >= 0);
    return emst_is_in_circle(
        p, coords[p], coords[p + 1],
        q, coords[q], coords[q + 1],
        r, coords[r], coords[r + 1],
        s, coords[s], coords[s + 1]);
}
