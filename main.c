/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "emst.h"

static void print_help()
{
    printf("emst - compute Euclidean minimum spanning tree\n\n");
    printf("Usage:\n");
    printf("    emst [input [output]]\n\n");
    printf("Input format:\n");
    printf("    n_points x0 y0 ... x{n-1} y{n-1}\n");
    printf("(points must be integers between -8192 and 8191,\n");
    printf("line breaks don't matter)\n\n");
    printf("Output format:\n");
    printf("    from0 to0\n");
    printf("    from1 to1\n");
    printf("    ...\n");
    printf("    from{n-2} to{n-2}\n");
    printf("Here from{i} and to{i} are endpoint indices of the i-th arc.\n");
}

static void do_emst(FILE *in, FILE *out)
{
    // read the input
    int n;
    if (fscanf(in, "%d", &n) != 1 || n < 0)
    {
        fprintf(stderr, "Invalid number of points\n");
        exit(EXIT_FAILURE);
    }

    int16_t *points = (int16_t *) malloc(2 * n * sizeof(int16_t));
    if (!points)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 2 * n; i++)
    {
        int k;
        if (fscanf(in, "%d", &k) != 1)
        {
            fprintf(stderr, "IO error: unable to read a coordinate\n");
            exit(EXIT_FAILURE);
        }
        if (k < -8192 || k > 8191)
        {
            fprintf(stderr, "Coordinate out of range (-8192..8191)\n");
            exit(EXIT_FAILURE);
        }
        points[i] = k;
    }

    // proceed
    int *edges = emst(points, n);

    for (int i = 0; i < n - 1; i++)
    {
        fprintf(out, "%d %d\n", edges[2 * i], edges[2 * i + 1]);
    }
    free(points);
    if (edges)
        free(edges);
}


int main(int argc, char **argv)
{
    FILE *in = stdin;
    FILE *out = stdout;

    if (argc > 3 || (argc > 1 && 
           (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))))
    {
        print_help();
        exit(EXIT_SUCCESS);
    }

    if (argc > 1)
        in = fopen(argv[1], "r");
    if (!in)
    {
        perror(argv[1]);
        exit(EXIT_FAILURE);
    }
    if (argc > 2)
        out = fopen(argv[2], "w");
    if (!out)
    {
        perror(argv[2]);
        exit(EXIT_FAILURE);
    }

    do_emst(in, out);

    if (in != stdin)
        fclose(in);
    if (out != stdout)
        fclose(out);

    return EXIT_SUCCESS;
}
