/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef EMST_H
#define EMST_H

#include <stdint.h>

/**
 * \file emst.h
 *
 * This is an implementation of exact arithmetic Delaunay triangulator.
 *
 *
 * References
 *
 *   [Kn] D. E. Knuth, "Axioms and Hulls", Heidelberg: Springer-Verlag, 1992
 *        http://www-cs-faculty.stanford.edu/~uno/aah.html
 *
 *   [GS] L. Guibas, J. Stolfi, "Primitives for the manipulation of general 
 *        subdivisions and the computation of Voronoi", ACM Trans. Graph., 
 *        Vol. 4, Issue 2, April 1985.
 *        http://doi.acm.org/10.1145/282918.282923
 *
 *   [Sh] J. R. Shewchuk, "Triangle: Engineering a 2D Quality Mesh Generator 
 *        and Delaunay Triangulator", in "Applied Computational Geometry:
 *        Towards Geometric Engineering", ed. Ming C. Lin and D. Manocha,
 *        Lecture Notes in Computer Science, Vol. 1148, pp. 203-222, May 1996.
 *
 *        Of particular interest is the chapter "Triangulation Algorithms and 
 *        Data Structures", accessible at
 *        http://www.cs.cmu.edu/~quake/tripaper/triangle2.html
 * 
 * This implementation generally follows [GS]. A graph of triangles described 
 * in [Sh] is used instead of quad-edge structure from [GS]. ICircle and CCW 
 * tests are taken from [Kn] to prevent special cases. Alternate vertical and
 * horizontal splitting recommended in [Sh] is NOT implemented.
 */


/**
 * A face of a trianglular mesh.
 *
 * A triangle consists of 3 vertices and 3 edges, 
 * labeled 0, 1, and 2 counterclockwise.
 * The i-th vertex lies across the i-th edge.
 *
 * \sa http://www.cs.cmu.edu/~quake/tripaper/triangle2.html
 */
typedef struct
{
    /**
     * Indexes of the vertices in the point cloud.
     * A virtual point at infinity has the index of -1. 
     */
    int vertices[3];

    /**
     * Indexes of neighboring triangles that lie across the given edges.
     */
    int edges_t[3];   // opposite to the points with the corresponding indices

    /**
     * Indexes that the same edge has in the neighboring triangle (0, 1, 2).
     */
    char edges_i[3];
} EmstTriangle;

/**
 * Compute Delaunay triangulation.
 *
 * \param[in] coords    n_points coordinate pairs, so 2 * n_points items.
 *                      The valid input ranges from -8192 to 8191.
 *                      
 * \returns     (2 * n_points - 2) triangles, to be cleaned up with free()
 */
EmstTriangle *emst_delaunay(const int16_t *coords, int n_points);

/**
 * Find minimum spanning tree with Kruskal algorithm on an arbitrary graph.
 * The input graph must be connected.
 *
 * \returns    The array of v - 1 (begin, end) pairs, so 2 * v - 2 items.
 */
int *emst_kruskal(
    int v, 
    int e, 
    int *restrict from,
    int *restrict to, 
    int *restrict dist);

/**
 * Find Euclidean minimum spanning tree.
 *
 * \param coords    2 * n_points coordinates in (x, y) pairs
 * \returns         n_points - 1 edges as (begin, end) pairs;
 *                  so 2 * n_points - 2 integers in total.
 *                  Returns NULL for n_points <= 1.
 */
int *emst(const int16_t *coords, int n_points);

#endif
