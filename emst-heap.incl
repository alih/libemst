/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


/**
 * This is what the poor C programmers do instead of C++ templates ;)
 * 
 * Include this file to implement a heap:
 *
 *   #define EMST_HEAP_IS_ABOVE(A, B) ...
 *   #define EMST_HEAP_SWAP(A, B) ...
 *   #define EMST_HEAP_BUILD_PROTO ...
 *   #define EMST_HEAP_EXTRACT_PROTO ...
 *   #include "emst-heap.incl"
 *
 * Use `priorities' as the name for the array of keys and `n' for its length.
 */

#ifndef EMST_HEAP_IS_ABOVE
#define EMST_HEAP_IS_ABOVE(A, B) (priorities[A] > priorities[B]) // max heap
#endif

#ifndef EMST_HEAP_SWAP
#define EMST_HEAP_SWAP(A, B) do { int tmp = priorities[A]; \
                              priorities[A] = priorities[B]; \
                              priorities[B] = tmp; } while(0)
#endif

#ifndef EMST_HEAP_BUILD_PROTO
#define EMST_HEAP_BUILD_PROTO static void heap_build(int *priorities, int n)
#endif

#ifndef EMST_HEAP_EXTRACT_PROTO
#define EMST_HEAP_EXTRACT_PROTO static void heap_extract(int *priorities, int n)
#endif


EMST_HEAP_BUILD_PROTO
{
    for (int k = 0; k < n; k++)
    {
        // sift k up
        int i = k;
        while (i > 0)
        {
            int parent = (i - 1) / 2;
            if (EMST_HEAP_IS_ABOVE(i, parent))
                EMST_HEAP_SWAP(i, parent);
            else
                break;

            i = parent;
        }
    }
}


EMST_HEAP_EXTRACT_PROTO
{
    EMST_HEAP_SWAP(0, n - 1);
    n--;
    
    // sift down
    int i = 0;
    int i_max = n / 2;
    while (i < i_max)
    {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        
        if (right >= n)
        {
            // there's only one child of this node
            if (EMST_HEAP_IS_ABOVE(left, i))
                EMST_HEAP_SWAP(i, left);
            return;
        }

        // the best of i, left and right gets to i
        int right_above = EMST_HEAP_IS_ABOVE(right, left);

        if (right_above && EMST_HEAP_IS_ABOVE(right, i))
        {
            EMST_HEAP_SWAP(i, right);
            i = right;
        }
        else if (!right_above && EMST_HEAP_IS_ABOVE(left, i))
        {
            EMST_HEAP_SWAP(i, left);
            i = left;
        }
        else
            return;
    }
}

#undef EMST_HEAP_IS_ABOVE
#undef EMST_HEAP_SWAP
#undef EMST_HEAP_BUILD_PROTO
#undef EMST_HEAP_EXTRACT_PROTO

// vim: set ft=c:
