# vim: set ft=python:
VERSION = '0.1'
APPNAME = 'emst'

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')

def configure(conf):
    conf.load('compiler_c')
    conf.env.append_value('CFLAGS', ['-std=c99',
        '-Wall', '-Wextra', '-Wshadow', '-Waggregate-return', '-Wcast-align',
        '-Wmissing-prototypes', '-Wpointer-arith', 
        '-Wredundant-decls', '-Wcast-qual', '-Wmissing-declarations'])

    conf.env.append_value('CFLAGS', ['-O3', '-DNDEBUG'])

def build(bld):
    bld.shlib(target = APPNAME, vnum = '0.0.0', 
              source = 'emst-delaunay.c emst-geometry.c emst-kruskal.c')
    bld.program(target = './emst', source = 'main.c', use = APPNAME)
    bld.install_files('${PREFIX}/include', 'emst.h')
