/*
 * libemst - a library for computing Euclidean minimum spanning trees in 2D
 * Copyright (C) 2010-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "emst.h"

#define EMST_HEAP_IS_ABOVE(A, B) (priorities[A] < priorities[B])
#define EMST_HEAP_SWAP(A, B) \
    do { \
        int t =   from[A];       from[A] =       from[B];       from[B] = t; \
        t =         to[A];         to[A] =         to[B];         to[B] = t; \
        t = priorities[A]; priorities[A] = priorities[B]; priorities[B] = t; \
    } while (0)
#define EMST_HEAP_BUILD_PROTO \
    static void kruskal_heap_build(int *from, int *to, int *priorities, int n)
#define EMST_HEAP_EXTRACT_PROTO \
    static void kruskal_heap_extract(int *from, int *to, int *priorities, int n)
#include "emst-heap.incl"

static void disjoint_set_init(int *parent, int *ranks, int n)
{
    memset(parent, 255, n * sizeof(int));
    memset(ranks, 0, n * sizeof(int));
}

// Merge method of the Disjoint-set data structure
// a and b must be roots
static void merge(int *parent, int *ranks, int a, int b)
{
    if (ranks[a] > ranks[b])
        parent[b] = a;
    else if (ranks[a] < ranks[b])
        parent[a] = b;
    else
    {
        // merging roots with equal ranks: rank goes up
        parent[b] = a;
        ranks[a]++;
    }
}

// Find method of the Disjoint-set data structure
static int find(int *parent, int k)
{
    int i = k;
    while (parent[i] >= 0)
        i = parent[i];
    
    // path compression
    while (k != i)
    {
        int new_k = parent[k];
        parent[k] = i;
        k = new_k;
    }
    
    return i;
}

int *emst_kruskal(
    int v, 
    int e, 
    int *restrict from,
    int *restrict to, 
    int *restrict dist)
{
    if (v <= 1)
        return NULL;

    int *result = (int *) malloc(2 * (v - 1) * sizeof(int));
    int *parents = (int *) malloc(v * sizeof(int));
    int *ranks = (int *) malloc(v * sizeof(int));
    disjoint_set_init(parents, ranks, v);
    kruskal_heap_build(from, to, dist, e);
    
    int counter = 0;
    int counter_cap = 2 * (v - 1);
    do
    {
        int begin;
        int end;
        int f;
        int t;
        do
        {
            kruskal_heap_extract(from, to, dist, e--);
            begin = from[e];
            end = to[e];

            f = find(parents, begin);
            t = find(parents, end);
        } while (f == t);
            
        merge(parents, ranks, f, t);
        result[counter++] = begin;
        result[counter++] = end;
    } while (counter < counter_cap);

    free(parents);
    free(ranks);
    return result;
}

static void connect(int a, int b, const int16_t *coords, 
                    int *from, int *to, int *dist, int *ref_e)
{
    if (a >= b || a < 0)
        return;
    from[*ref_e] = a;
    to[*ref_e] = b;
    int dx = coords[2 * a] - coords[2 * b];
    int dy = coords[2 * a + 1] - coords[2 * b + 1];

    dist[*ref_e] = dx * dx + dy * dy;
    (*ref_e)++;
}

int *emst(const int16_t *coords, int n_points)
{
    if (n_points < 2)
        return NULL;

    EmstTriangle *triangles = emst_delaunay(coords, n_points);
    int n_triangles = 2 * n_points - 2;
     
    int e = 0;
    int e_max = 3 * n_points - 3; // E = 3V - 6 = 3(n_points + 1) - 6
    int *from = (int *) malloc(e_max * sizeof(int));
    int *to =   (int *) malloc(e_max * sizeof(int));
    int *dist = (int *) malloc(e_max * sizeof(int));
    for (int i = 0; i < n_triangles; i++)
    {
        const int *vertices = triangles[i].vertices;
        connect(vertices[0], vertices[1], coords, from, to, dist, &e);
        connect(vertices[1], vertices[2], coords, from, to, dist, &e);
        connect(vertices[2], vertices[0], coords, from, to, dist, &e);
    }

    free(triangles);

    int *result = emst_kruskal(n_points, e, from, to, dist);

    free(from);
    free(to);
    free(dist);
    return result;
}
